
from django.contrib.auth.models import User
from quiz.models import Category
from quiz.models import SubCategory
from quiz.models import Quiz
from multichoice.models import MCQuestion, Answer


def initConfiguraciones():
    #if(not User.objects.filter(username = 'Yurs')):
    if(True):

        print("configurando")

        admin = User.objects.create_superuser(username='Yurs', email='Yurs@admin.com' ,password='yurleysanchez', first_name="YURLEY KATTERINE", last_name="SANCHEZ FLOREZ")
        admin1 = User.objects.create_superuser(username='Reyez', email='Reyez@admin.com' ,password='SAIYAJIN09', first_name="OSCAR JESÚS", last_name="REYES PEÑA")
        admin2 = User.objects.create_superuser(username='Mac', email='Mac@admin.com' ,password='Mac31', first_name="MAC GRÉGOR", last_name="HERRERA ANAYA")


        ct0 = Category()
        ct0.category = "Ciencias Naturales"
        ct0.save()

        ct1 = Category()
        ct1.category = "Ciencias Sociales"
        ct1.save()

        ct2 = Category()
        ct2.category = "Lenguaje"
        ct2.save()

        ct3 = Category()
        ct3.category = "Matemáticas"
        ct3.save()


        sct0 = SubCategory()
        sct0.sub_category = "Reino Animal"
        sct0.category = ct0
        sct0.save()

        sct0 = SubCategory()
        sct0.sub_category = "Anatomia"
        sct0.category = ct0
        sct0.save()

        sct0 = SubCategory()
        sct0.sub_category = "Historia"
        sct0.category = ct1
        sct0.save()

        sct0 = SubCategory()
        sct0.sub_category = "Politica"
        sct0.category = ct1
        sct0.save()

        sct0 = SubCategory()
        sct0.sub_category = "Semantica"
        sct0.category = ct2
        sct0.save()

        sct0 = SubCategory()
        sct0.sub_category =  "Ortografia"
        sct0.category = ct2
        sct0.save()

        sct0 = SubCategory()
        sct0.sub_category = "Trigonometria"
        sct0.category = ct3
        sct0.save()

        sct0 = SubCategory()
        sct0.sub_category = "Algebra"
        sct0.category = ct3
        sct0.save()



        qq0 = Quiz()
        qq0.title = "Prueba de Ciencias Naturales"
        qq0.description = "Prueba de Biologia con Vida"
        qq0.url = "CienciasNaturales"
        qq0.category = ct0
        qq0.random_order = True
        qq0.max_questions = 5
        qq0.answers_at_end = True
        qq0.exam_paper = True
        qq0.single_attempt = True
        qq0.pass_mark = 3
        qq0.success_text = "Muy Bien"
        qq0.fail_text = "Debes estudiar un poco más"
        qq0.draft = False
        qq0.save()

        qq1 = Quiz()
        qq1.title = "Prueba de Ciencias Sociales"
        qq1.description = "Prueba de Sociales con historias"
        qq1.url = "CienciasSociales"
        qq1.category = ct1
        qq1.random_order = True
        qq1.max_questions = 5
        qq1.answers_at_end = True
        qq1.exam_paper = True
        qq1.single_attempt = True
        qq1.pass_mark = 3
        qq1.success_text = "Muy Bien"
        qq1.fail_text = "Debes estudiar un poco más"
        qq1.draft = False
        qq1.save()

        qq2 = Quiz()
        qq2.title = "Prueba de Lenguaje"
        qq2.description = "Prueba de Español con letras"
        qq2.url = "Lenguaje"
        qq2.category = ct2
        qq2.random_order = True
        qq2.max_questions = 5
        qq2.answers_at_end = True
        qq2.exam_paper = True
        qq2.single_attempt = True
        qq2.pass_mark = 3
        qq2.success_text = "Muy Bien"
        qq2.fail_text = "Debes estudiar un poco más"
        qq2.draft = False
        qq2.save()

        qq3 = Quiz()
        qq3.title = "Prueba de Matematicas"
        qq3.description = "Prueba de Matematicas con numeritos"
        qq3.url = "Matematicas"
        qq3.category = ct3
        qq3.random_order = True
        qq3.max_questions = 5
        qq3.answers_at_end = True
        qq3.exam_paper = True
        qq3.single_attempt = True
        qq3.pass_mark = 3
        qq3.success_text = "Muy Bien"
        qq3.fail_text = "Debes estudiar un poco más"
        qq3.draft = False
        qq3.save()


        lista = [
            ["7750", "ANAYA.GISSELLE", "ANAYA SUAREZ", "GISSELLE MELISSA", "7750"],
            ["4601", "BAEZ.DAIRON", "BAEZ TRUJILLO", "DAIRON STEVEN", "4601"],
            ["9511", "BAYONA.KAREN", "BAYONA OCHOA", "KAREN YURLEY", "9511"],
            ["9549", "BECERRA.MAURICIO", "BECERRA GAMEZ", "MAURICIO ", "9549"],
            ["8168", "BUENO.JHON", "BUENO PEREZ", "JHON STIVEN", "8168"],
            ["4118", "CARVAJALINO.MELISSA", "CARVAJALINO MANOSALVA", "MELISSA FERNANDA", "4118"],
            ["6094", "CASTILLO.ELKIN", "CASTILLO AMAYA", "ELKIN ANDREY", "6094"],
            ["8257", "CASTRO.KAROL", "CASTRO AGUDELO", "KAROL DAYANA", "8257"],
            ["9101", "FERNANDEZ.LAURA", "FERNANDEZ VARGAS", "LAURA JULIETH", "9101"],
            ["2768", "FIGUEROA.DARELIS", "FIGUEROA PIGO", "DARELIS SARAY", "2768"],
            ["5316", "GOMEZ.JOSE", "GOMEZ CAMACHO", "JOSE MIGUEL", "5316"],
            ["8121", "GUEVARA.JUAN", "GUEVARA HERNANDEZ", "JUAN DANIEL", "8121"],
            ["1893", "HERNANDEZ.KAREN", "HERNANDEZ MENDOZA", "KAREN JUUETH", "1893"],
            ["2935", "HERNANDEZ.JHON", "HERNANDEZ MORALES", "JHON ALEXANDER", "2935"],
            ["1916", "HERRERA.VALERIA", "HERRERA ANAYA", "VALERIA DAYANNA", "1916"],
            ["6859", "LAZARO.ANGEL", "LAZARO MORA", "ANGEL YESID", "6859"],
            ["1987", "LEON.JESSICA", "LEON GOMEZ", "JESSICA ", "1987"],
            ["6797", "MANTILLA.DANIEL", "MANTILLA BUENO", "DANIEL FERNANDO", "6797"],
            ["10740", "MUÑOZ.NATALIA", "MUÑOZ VALERO", "NATALIA ", "10740"],
            ["7369", "NEIRA.MARIA", "NEIRA NEIRA", "MARIA ALEJANDRA", "7369"],
            ["2266", "NIEVES.KAROL", "NIEVES VIRVIESCAS", "KAROL TATIANA", "2266"],
            ["6789", "PEÑA.JHOAN", "PEÑA JAIMES", "JHOAN STEVEN", "6789"],
            ["5321", "PONCE.GERALDINE", "PONCE SILVA", "GERALDINE NICOLE", "5321"],
            ["8982", "QUINTERO.KEVIN", "QUINTERO GALINDO", "KEVIN GEOVANNY", "8982"],
            ["11049", "QUINTERO.GARBIELA", "QUINTERO PARRA", "GABRIELA ISABEL", "11049"],
            ["6896", "QUINTERO.TATIANA", "QUINTERO RUEDA", "TATIANA LIZETH", "6896"],
            ["9842", "RINCON.BRAYDER", "RINCON BAYONA", "BRAYDER JOHAN", "9842"],
            ["6891", "ROBLES.KAROL", "ROBLES ARCINIEGAS", "KAROL DAYANA", "6891"],
            ["8803", "RODRIGUEZ.JULIANA", "RODRIGUEZ DURAN", "JULIAN ALEXANDER", "8803"],
            ["9216", "ROJAS.MARIA", "ROJAS CAPACHO", "MARIA FERNANDA", "9216"],
            ["8017", "RUEDA.DEICY", "RUEDA AMADO", "DEICY CAROLINA", "8017"],
            ["2001", "SAMACA.GUSTAVO", "SAMACA CELIS", "GUSTAVO ANDRES", "2001"],
            ["2267", "SANDOVAL.JENNIFER", "SANDOVAL CORREDOR", "JENNIFER PAOLA", "2267"],
            ["8665", "SARMIENTO.JESUS", "SARMIENTO JEREZ", "JESUS ALBERTO", "8665"],
            ["5023", "SEPULVEDA.BRAYAN", "SEPULVEDA MARIN", "BRAYAN GERARDO", "5023"],
            ["5589", "SERRANO.JHONATAN", "SERRANO ACEVEDO", "JHONATAN DAVID", "5589"],
            ["10849", "SILVA.JUAN", "SILVA BARBOSA", "JUAN CAMILO", "10849"],
            ["6796", "VALENCIA.KAREN", "VALENCIA CORREA", "KAREN DAYANA", "6796"],
            ["9415", "VILLAREAL.LAURA", "VILLARREAL LIZCANO", "LAURA CAMILA", "9415"]
        ]
        for i in lista:
            usuario = User.objects.create_user(username=i[1],
                                               password=i[0],
                                               first_name =i[3],
                                               last_name =i[2],
                                               email = i[0] + "@mail.com",
                                              )


        listaq =    [["Ciencias Naturales", "El nombre científico de la planta medicinal conocida popularmente como 'Canela' es:", "Cinnamomum verum.", "Aloe Vera.", "Mentha spicata.", "Tectona grandis", "Cinnamomum zeylanicum o Cinnamomum verum es el nombre científico del canelo o canela, una planta medicinal preciada por su corteza."],
["Ciencias Naturales", "¿Qué es la Taxonomía?", "Es, generalmente, la ciencia de la clasificación.", "El estudio del Sarcophilus harrisii, popularmente conocido como Demonio de Tasmania.", "Una rama de la economía dedicada a la inspección de la facturación en el sector comercial. Del inglés (Tax = Impuesto).", "Método utilizado por los programadores para evaluar el costo anualmente que deben tener el servicio de taxis en una ciudad determinada.", "La Taxonomía (del griego táxis ‘ordenamiento’ y nómos ‘norma’ o ‘regla’) es, en su sentido más general, la ciencia de la clasificación. Habitualmente se emplea el término para designar a la Taxonomía Biológica."],
["Ciencias Naturales", "¿Cuál de los siguientes compuestos es un ácido?", "HCl.", "NaCl.", "CO.", "He", "HCl, corresponde a un Ácido (Ácido Clorhídrico), NaCl correponde a una Sal (Cloruro de Sodio), CO corresponde a un Óxido (Monóxido de Carbono) y He es el símbolo químico del Helio."],
["Ciencias Naturales", "¿Cuál de los siguiente individuos NO pertenece a la clase 'Insecta' (Los Insectos)?", "La tarántula (Lycosa tarantula).", "El avispón (Vespa crabro).", "La mariposa emperador (Thysania agrippina).", "La mosca doméstica (Musca domestica).", "Las arañas pertenecen a la clase Arachnida (Los Arácnidos) junto con las garrapatas y los escorpiones, entre otros."],
["Ciencias Naturales", "¿Cual de los siguientes elementos es un no metal?", "Carbono.", "Sodio.", "Potasio.", "Mercurio.", "El carbono es un elemento químico con símbolo C, número atómico 6 y masa atómica 12,01. Es no metal y tetravalente."],
["Ciencias Naturales", "¿Cuántos Periodos tiene la tabla periódica de los elementos Químicos?", "Siete.", "Dieciocho.", "Seis.", "Once.", "La tabla periódica consta de siete periodos y dieciocho grupos. Siendo los periodos la filas de la misma y los grupos las columnas."],
["Ciencias Naturales", "¿A qué reino pertenecen las Amebas?", "Protista.", "Mónera.", "Fungi.", "Plantae.", "Ameba o amiba es un protista unicelular del género Amoeba."],
["Ciencias Naturales", "¿Cuál de los siguientes elementos es un gas noble?", "Kriptón.", "Holmio.", "Estroncio.", "Bismuto.", "El kriptón es un gas noble inodoro e insípido de poca reactividad caracterizado por un espectro de líneas verde y rojo-naranja muy brillantes."],
["Ciencias Naturales", "¿Qué son los Electrones de Valencia?", "Son los electrones que tiene un átomo en su último nivel de energía.", "Son los electrones que giran en sentido contrario a los demás, llamados así por Fredericc Valencia, quien descubrió esta peculiaridad en 1981.", "Son los protones que invierten su carha y se convierten en electrones.", "Son los electrones con mayor valor energético en el átomo.", "Los electrones de valencia son los electrones que se encuentran en la capa de mayor nivel de energía del átomo."],
["Ciencias Naturales", "¿Cuál de las siguiente es una propiedad química de la materia?", "Oxidación.", "Punto de fusión.", "Punto de ebullición.", "Densidad.", "La propiedades químicas son aquellas propiedades que tienen las distintas sustancias de poder transformarse o modificarse, a través de reacciones químicas. La oxidación por ejemplo, pues al ocurrir ésta, la composición química del elemento cambia: El hiero (Fe) al oxidarse produce óxido de hierro (FeO)."],
["Ciencias Naturales", "¿Qué es una Propiedad INTENSIVA de la Materia?", "Es aquella que no depende de la masa o de la geometría de un cuerpo.", "Es aquella que sí depende de la masa o de la geometría de un cuerpo.", "Es aquella propiedad por la cual la materia cambia su composición química.", "Es aquella propiedad que mide la relación entre masa y volumen de un cuerpo.", "Las propiedades intensivas son aquellas que no dependen de la masa o del tamaño de un cuerpo, por lo que el valor permanece inalterable al dividir el sistema inicial en varios subsistemas, por este motivo no son propiedades aditivas."],
["Ciencias Naturales", "¿Qué es una Propiedad EXTENSIVA de la materia?", "Es aquella que sí depende de la masa o de la geometría de un cuerpo.", "Es aquella que no depende de la masa o de la geometría de un cuerpo.", "Es aquella propiedad por la cual la materia cambia su composición química.", "Es aquella propiedad que mide la relación entre masa y volumen de un cuerpo.", "Las propiedades extensivas son aquellas que sí dependen de la masa o del tamaño de un cuerpo, son magnitudes cuyo valor es proporcional al tamaño del sistema que describe."],
["Ciencias Naturales", "¿Qué es una Propiedad Química de la materia?", "Es aquella propiedad por la cual la materia cambia su composición química.", "Es aquella propiedad que mide la relación entre masa y volumen de un cuerpo.", "Es aquella que sí depende de la masa o de la geometría de un cuerpo.", "Es aquella que no depende de la masa o de la geometría de un cuerpo.", "Una propiedad química es cualquier propiedad de la materia por la cual un cuerpo cambia de composición. Por ejemplo; el estado de oxidación."],
["Ciencias Naturales", "¿Cuál de las siguientes no es una parte de la célula animal?", "Pared celular.", "Mitocondria.", "Ribosomas.", "Vacuolas.", "La pared celular es una capa resistente, a veces rígida, porque soporta las fuerzas osmóticas y el crecimiento, que se localiza en el exterior de la membrana plasmática en las células de plantas, hongos, algas, bacterias y arqueas."],
["Ciencias Naturales", "¿Cuál es la diferencia fundamental entre las células Procariotas y Eucariotas?", "La presencia de un núcleo definido que envuelve el material genético.", "Las células procariotas son células vegetales y las célular eucariotas son células animales.", "Las células procariotas necesitan del oxigeno para sobrevivir, y las eucariotas no depende del oxígeno para su supervivencia.", "No tienen diferencia alguna.", "Se llama células eucariotas a las que tienen un citoplasma, compartimentado por membranas, destacando la existencia de un núcleo celular organizado, se distinguen así de las células procariotas que carecen de núcleo definido, por lo que el material genético se encuentra disperso en su citoplasma."],
["Ciencias Naturales", "¿Cuál de las siguiente NO es una parte de la célula vegetal común?", "Flagelo.", "Núcleo.", "Citoplasma.", "Cloroplastos.", "La estructura de la célula vegetal está compuesta por: Pared celular, Citoplasma, Plasmodesmo, Vacuola, Cloroplastos, Leucoplastos, Cromoplastos, Aparato de Golgi, Ribosomas, Retículo endoplasmático, Mitocondrias, Membrana celular de la célula vegetal, Citoplasma y Núcleo celular."],
["Ciencias Naturales", "Los enlaces covalentes pueden ser:", "Polar o Apolar.", "Iónico o Catiónico.", "Positivo o Negativo.", "Electrónico o Protónico.", "Dependiendo de la diferencia de electronegatividad, el enlace covalente puede ser clasificado en covalente polar y covalente puro o apolar."],
["Ciencias Naturales", "¿Cuál de los siguientes NO es un símbolo químico de un elemento de la Tabla Periódica?", "Jw.", "Tl.", "Sm.", "Am.", "La respuesta es Jw. Tl corresponde al Talio, número atómico 81, Sm corresponde al Samario, número atómico 62 y Am corresponde al Americio, número atómico 95. Como dato adicional; ¿sabías que la letra J no aparece en ningún símbolo químico de la tabla periódica?."],
["Ciencias Naturales", "¿Cuáles son las unidades genéticas encargadas de transmitir un carácter hereditario?", "Genes.", "Fenotipos.", "Alelos.", "Cromosomas.", "Un gen es una unidad de información en un locus de ácido desoxirribonucleico (ADN) que codifica un producto funcional, es decir: es la unidad molecular de la herencia."],
["Ciencias Naturales", "La nomenclatura binomial emplea dos palabras del Latín para identificar un individuo, ¿A qué corresponden estas dos palabras?", "El Género y la Especie.", "El Reino y la Especie.", "La Familia y el Género.", "El Filo y la Especie.", "Como sugiere la palabra 'binominal', el nombre científico otorgado a una especie está formado por la combinación de dos palabras: el nombre del género y el epíteto o nombre específico de la especie."],
["Ciencias Sociales", "¿Cuál de los siguientes volcanes pertenecen al territorio colombiano?", "Volcán Chiles.", "Volcán El Chimborazo.", "Volcán Villarrica.", "Volcán Ollagüe", "El Chiles es un volcán nevado en el Nudo de los Pastos, en la cordillera occidental andina colombiana."],
["Ciencias Sociales", "¿Qué país de los siguientes es fronterizo con Colombia?", "Perú.", "Argentina.", "Bolivia.", "Costa Rica", "Los paises fronterizos con el territorio Colombiano son: Ecuador, Perú, Brasil, Venezuela y Panamá."],
["Ciencias Sociales", "¿Qué periodo ocupó el gobierno militar de Gustavo Rojas Pinilla?", "de 1953 a 1957.", "de 1945 a 1950.", "de 1950 a 1953.", "de 1939 a 1945", "El 13 de junio de 1953, Rojas Pinilla realizó un golpe sin derramamiento de sangre: ninguna persona murió. En la madrugada del 10 de mayo de 1957 Rojas aceptó retirarse y en su sustitución nombró un gobierno militar de transición."],
["Ciencias Sociales", "¿A qué se le llama 'El Bogotazo'?", "A la revuelta popular como consecuencia del magnicidio del jurista, escritor, político y candidato presidencial Jorge Eliécer Gaitán el 9 de abril de 1948.", "A la fecha en que se fundó Santa Fe de Bogotá.", "A la elección de Jorge Eliécer Gaitán como alcalde de Bogotá en el año 1936.", "Al asesinato de Luis Carlos Galán el 18 de agosto de 1989 poco antes de comenzar su discurso en un evento público electoral en el municipio de Soacha, Cundinamarca.", "Tras el asesinato de Jorge Eliécer Gaitán el 9 de abril de 1948, presuntamente a manos de Juan Roa Sierra, en el centro de Bogotá, la ciudad fue sacudida fuertemente por protestas violentas, represión y desórdenes que la dejaron  casi destruida, y que se expandió a otras regiones y ciudades del país. Suceso conocido popularmente como 'El Bogotazo' por  ser Bogotá la ciudad donde todo se desencadenó."],
["Ciencias Sociales", "¿Cuál de los siguientes NO ha sido un nombre del territorio Nacional?", "Confederación Colombiana.", "Gran Colombia.", "Estados Unidos de Colombia.", "Nueva Granada.", "El Territorio nacional ha tenido la siguiente denominación, cronológicamente: Provincias Unidas, Gran Colombia, Nueva Granada, Confederación Granadina, Estados Unidos de Colombia y finalmente República de Colombia."],
["Ciencias Sociales", "¿Cuál de los siguientes NO es un departamento de Colombia? ", "Valledupar.", "Chocó.", "Casanare.", "Cesar.", "Departamento de Cesar, cuya capital es la ciudad de Valledupar."],
["Ciencias Sociales", "De los siguientes personajes, ¿Cuál NO fue un presidente de La República de Colombia?", "Jorge Eliécer Gaitán.", "Alberto Lleras Camargo.", "César Gaviria.", "Julio César Turbay.", "Jorge Eliecer Gaitán Ayala fue un jurista, escritor y político colombiano. Fue alcalde de Bogotá en el año 1936, titular en dos ministerios (Educación en 1940 y Trabajo en 1944) y congresista durante varios períodos entre 1929 y 1948. También fue candidato presidencial disidente del Partido Liberal en las elecciones de 1946 quedando de último en la sumatoria de votos, siendo superado por Gabriel Turbay Abunader y Mariano Ospina Pérez; asumiendo éste último el cargo de presidente."],
["Ciencias Sociales", "¿En qué año se llevó a cabo la independencia de Panamá del territorio Colombiano?", "1903.", "1910.", "1900.", "1913.", "La separación de Panamá de Colombia​ fue un hecho ocurrido el 3 de noviembre de 1903, después de la Guerra de los Mil Días, y que desencadenó la proclamación de la República de Panamá."],
["Ciencias Sociales", "¿Qué hecho desencadenó la época conocida como 'La Violencia' en Colombia?", "El Bogotazo.", "El Golpe de Estado de Rojas Pinilla.", "La muerte de Luis Carlos Galán.", "El Mandato de La Junta Militar.", "La ola de protestas violentas durante el Bogotazo desencadenó una de las épocas más relevantes del siglo XX en la historia de Colombia, conocida como 'La Violencia'."],
["Ciencias Sociales", "¿Qué departamento de Colombia tiene límite tanto con el océano Atlántico como el Pacífico?", "Chocó.", "Valle del Cauca.", "La Guajira.", "Antioquia.", "Chocó está ubicado al noroeste del país, en las regiones andina y Pacífico, limitando al norte con Panamá y el mar Caribe (océano Atlántico), al noreste con Antioquia, al este con Risaralda, al sur con Valle del Cauca y al oeste con el océano Pacífico."],
["Ciencias Sociales", "¿Cuál de las siguientes ciudades NO pertenece a la región costera de Colombia (Región Caribe y Región Pacífica)?", "Tunja.", "Montería.", "Sincelejo.", "Barranquilla.", "Tunja, capital de Boyacá, pertenece a la región Andina."],
["Ciencias Sociales", "¿Cuál de las siguientes cordilleras colombianas pertenece a la Cordillera de los Andes?", "Cordillera central, oriental y occidental.", "Solo la cordillera central.", "Solo la cordillera oriental.", "Solo la cordillera occidental.", "La Cordillera de los Andes cubre una parte considerable de Colombia. En este país los Andes se ramifican en tres vertientes:  La Cordillera Occidental, La Cordillera Central y La Cordillera Oriental."],
["Ciencias Sociales", "¿Cuál es el río más largo del territorio Colombiano?", "El Río Magdalena.", "El Río Cauca.", "El Río Orinoco.", "El Río Amazonas.", "Aunque los ríos Amazonas, Putumayo y Orinoco sean más largos que el Magdalena, gran parte de su longitud se encuentra fuera del territorio nacional, por lo que, el río Magdalena es el más largo en Colombia, extendiéndose 1528 km a lo largo del país."],
["Ciencias Sociales", "¿Cuántas regiones conforman la geografía Colombiana?", "Seis.", "Cinco.", "Cuatro.", "Siete.", "El territorio Colombiano está formado por seis regiones, la cuales son: Amazonia, Andina, Caribe, Insular, Orinoquía y Pacífico."],
["Ciencias Sociales", "Las banderas de Ecuador, Venezuela y Colombia tienen los mismo colores (amarillo, rojo y azul), ¿Cuál es la diferencia?", "La bandera de Colombia no lleva objetos en el centro.", "La bandera de Colombia lleva el escudo nacional en el centro.", "La bandera de Colombia lleva un arco de estrellas en el centro.", "Las franjas de la bandera de Colombia tienen el mismo tamaño.", "Aunque en algunas ocasiones se estiliza la bandera nacional ubicando el escudo de Colombia en el centro de ésta, La bandera oficial de la República de Colombia consta únicamente de las tres franjas amarilla, azul y roja en orden descendente, siendo la franja amarilla la de mayor área y sin ningún escudo o símbolo en su centro."],
["Ciencias Sociales", "¿Qué nombre recibe la cordillera que atraviesa el territorio Colombiano?", "Cordillera de los Andes.", "Cordillera de los Alpes.", "Cordillera del Atlas.", "Cordillera del Himalaya.", "Los Andes forman parte de los territorios de Argentina, Chile, Bolivia, Perú, Ecuador, Colombia y Venezuela. Su altura media ronda los 4000 metros y su punto más alto es el Aconcagua, cuyos 6960,8 m s. n. m. hacen de esta montaña la más alta del planeta fuera del sistema de los Himalayas."],
["Ciencias Sociales", "¿En qué hemisferio del planeta se encuentra ubicado el país?", "En ambos hemisferios.", "En el hemisferio sur.", "En el hemisferio norte.", "En ninguno de los dos hemisferios.", "El 70 % del territorio nacional se encuentra en el hemisferio norte y el otro 30% en el hemisferio sur."],
["Ciencias Sociales", "El río Amazonas y el Orinoco son las fronteras naturales de Colombia con:", "Perú y Venezuela.", "Brasil y Guyana.", "Perú y Brasil.", "Venezuela y Argentina.", "El río Amazonas es un río de América del Sur, que nace en Perú recorre la frontera Peruano-Colombiana y desemboca en Brasil. El río Orinoco nace en Venezuela, marca la frontera Venezolana con Colombia hasta la confluencia con el Meta y finalmente en dirección este-noreste hasta el océano Atlántico donde desemboca."],
["Ciencias Sociales", "¿Cuál es el pico más alto de Colombia?", "La Sierra Nevada de Santa Marta.", "La Sierra del Cocuy.", "El Volcán Galeras.", "El Aconcagua.", "Aunque el Aconcagua sea más alto que la Sierra Nevada de Santa Marta, está ubicado en Argentina, siendo entonces el Pico de Cristóbal Colón de la Sierra Nevada de Santa Marta el punto más elevado del relieve Colombiano con una altura de 5775 msnm."],
["Ciencias Sociales", "¿En qué fecha se llevó a cabo la independencia de Cartagena de Indias?", "11 de noviembre de 1811.", "20 de julio de 1810.", "12 de octubre de 1809.", "6 de agosto de 1800.", "La independencia de Cartagena se desarrolló en un periodo de diez años a partir del 11 de noviembre de 1811, fecha en la cual la ciudad declaró su independencia absoluta de España, constituyéndose en el primer territorio de la actual Colombia en declararse totalmente independiente."],
["Comprencion Lectora", "¿Cuál de las siguientes no forma parte de la estructura de un texto?", "Verbo.", "Título.", "Desarrollo.", "Conclusión.", "Un texto está compuesto, generalmente, de Título, Introducción, Desarrollo y Conclusión."],
["Comprencion Lectora", "¿Cuál de los siguiente no es un tipo de texto?", "Negativo.", "Argumentativo.", "Informativo.", "Narrativo.", "Los textos pueden ser Narrativos, Argumentativos, Instructivos, Expositivos, Descriptivos e Informativos."],
["Comprencion Lectora", "¿En qué parte del texto se desarrolla la información y se profundiza en el tema del texto?", "Desarrollo o cuerpo.", "Título.", "Introducción.", "Conclusión.", "Cuerpo: es una de las partes en las que se dan las explicaciones, se profundiza la información, se expone la idea, y puede contener Fotos, Gráficas para complementar la información escrita."],
["Comprencion Lectora", "El monte Everest es la montaña más alta del planeta Tierra, con una altura de 8848 metros (29 029 pies) sobre el nivel del mar. Está localizada en el continente asiático, en la cordillera del Himalaya, concretamente en la subcordillera de Mahalangur Himal; marca la frontera entre China y Nepal. El macizo incluye los picos vecinos Lhotse, 8516 m (27 940 pies); Nuptse, 7855 m (25 771 pies) y Changtse, 7580 m (24 870 pies). Según el párrafo anterior, podemos deducir que:", "Los habitantes de las zonas cercanas al Everest son, en su mayoría, asiáticos.", "El Everest mide 8848 metros.", "El Everest es el punto más alto del mundo.", "El Everest está ubicado en el Himalaya.", "La altura, ubicación y demás datos sobre el Everest es información implícita en el texto. Pero, al conocer la ubicación del mismo, podemos deducir información sobre sus pobladores cercanos."],
["Comprencion Lectora", "La gripe se transmite desde individuos infectados a través de gotas en aerosol cargadas de virus procedentes de secreción nasal, bronquial o saliva que contenga alguna de ellas, que son emitidas con la tos o los estornudos o sólo al hablar. Generalmente se requiere una distancia cercana (menor a un metro) con la persona enferma para ser infectado. Del texto anterior, podemos deducir que:", "La gripe no se transmite por contacto sanguíneo.", "La gripe es causada por una bacteria.", "La gripe no se transmite por saliva.", "La gripe no se puede contagiar al estar cerca de una persona contaminada con el virus.", "En el párrafo claramente se nombran las formas de contagio de la gripe, entre las cuales no está el contacto con sangre."],
["Comprencion Lectora", "La inmortalidad o vida eterna supone la existencia indefinida o infinita que consigue superar la muerte. A lo largo de la historia, los seres humanos han tenido el deseo de vivir para siempre. Según la oración anterior, podemos decir que:", "La inmortalidad es un ideal del hombre.", "La inmortalidad es un hecho, pero no para el hombre.", "La inmortalidad es un hecho para el hombre.", "La inmortalidad es un concepto demostrado científicamente.", "Al decir 'supone' el texto expresa que la idea de inmortalidad es una idea, sin pruebas ni argumentos al respecto."],
["Comprencion Lectora", "En física, un vector es una magnitud física definida en un sistema de referencia que se caracteriza por tener módulo y una dirección. Basados en el siguiente texto, es correcto decir que:", "Un vector necesita un sistema de referencia para ser representado.", "Un vector tiene únicamente longitud.", "Un vector tiene únicamente dirección.", "Un vector es un número cualquiera en un sistema de referencia.", "Un vector requiere un sistema de referencia en el cual ubicar su longitud (magnitud) y dirección (sentido) para poder representarlo correctamente."],
["Comprencion Lectora", "Entre las distintas manifestaciones de la pobreza figuran el hambre, la malnutrición, la falta de una vivienda digna y el acceso limitado a otros servicios básicos como la educación o la salud. También se encuentran la discriminación y la exclusión social, que incluye la ausencia de la participación de los pobres en la adopción de decisiones, especialmente de aquellas que les afectan. Teniendo en cuenta el texto anterior, es correcto afirmar que:", "La malnutrición es una señal de pobreza.", "La ausencia del servicio de televisión por cable, es una señal de pobreza.", "El hambre no tiene nada que ver con la pobreza.", "La exclusión social no es una manifestación de la pobreza.", "Una manifestación de la pobreza, hace referencia a las señales o consecuencias de ésta."],
["Comprencion Lectora", "Una buena educación es la base para mejorar nuestra vida y el desarrollo sostenible. En los últimos quinquenios, se han producido importantes avances con relación a la mejora en el acceso a la educación a todos los niveles y el incremento en las tasas de escolarización en las escuelas, sobre todo en el caso de las mujeres y las niñas. También se ha incrementado en gran medida el nivel mínimo de alfabetización. Según el párrafo anterior es posible deducir que:", "El desarrollo sostenible necesita de educación de calidad.", "La educación no es necesaria para mejorar nuestra vida y el desarrollo sostenible.", "Las tasas de escolarización han disminuido.", "El nivel mínimo de alfabetización se ha mantenido constante.", "Al ser la educación, la base de una mejora en pro de el desarrollo sostenible, ésta se hace indispensable para lograr un avance positivo en nuestra vida y el desarrollo sostenible."],
["Comprencion Lectora", "La sequía afecta a algunos de los países más pobres del mundo, recrudece el hambre y la desnutrición. Esa escasez de recursos hídricos, junto con la mala calidad del agua y el saneamiento inadecuado repercuten en la seguridad alimentaria, los medios de subsistencia y la oportunidad de educación para las familias pobres en todo el mundo. Un buen título para el párrafo anterior puede ser:", "Consecuencias de la falta de agua.", "La sequía.", "El hambre y la desnutrición.", "La pobreza en el mundo.", "La sequía, la pobreza, el hambre y la desnutrición, todas son consecuencias de la falta de agua potable."],
["Comprencion Lectora", "¿Cuál de las siguientes ideas es un hecho?", "El pasado lunes, ocurrió un accidente en la avenida más concurrida de la ciudad.", "Un testigo presume que el puente se cayó por problemas durante su construcción.", "Los pobladores cercanos creen que el volcán hará erupción pronto.", "La pesca en esta ciudad es buena probablemente debido a su ubicación geográfica.", "Un hecho es una afirmación que se puede comprobar (ocurrió a cierta hora, en cierto lugar, etc)."],
["Comprencion Lectora", "¿Cuál de las siguientes ideas es una opinión?", "La policía estima que en los próximos meses la delincuencia disminuirá.", "El pasado martes en la ciudad de Bogotá, el tren se salió de sus vías por falta de mantenimiento.", "En Holanda, la ola de calor que empezó desde hace ya cuatro meses, ha sido causa de pérdidas multimillonarias en la producción de flores.", "El pasado martes, aproximadamente a las 11 a.m. se llevó a cabo la captura del culpable del homicidio de un campesino local.", "Una opinión es un parecer (un pensamiento) que se apoya en las razones presentadas por la persona que expresa su forma de pensar."],
["Comprencion Lectora", "Los Objetivos de Desarrollo Sostenible (ODS) son un llamamiento mundial a la acción para poner fin a la pobreza, proteger el planeta y asegurar que todos los seres humanos disfruten de paz y prosperidad. Estos 17 objetivos globales comprenden 169 metas y orientarán las políticas y la financiación de los próximos 15 años. Sobre la base de los éxitos obtenidos mediante los Objetivos de Desarrollo del Milenio (ODM), los ODS incluyen nuevas esferas, como la desigualdad económica, la innovación, el cambio climático, el consumo sostenible, la paz y la justicia, entre otras. Los ODS son universales e inclusivos y reflejan un firme compromiso con las personas y el planeta. Según el texto anterior, es correcto afirmar que:", "Los ODS son un proyecto a futuro que quiere llevarse a cabo en todo el mundo.", "Los ODS son un proyecto nacional en proceso.", "Los ODS fueron un intento de eliminar la pobreza del mundo.", "Los ODS son un llamado mundial para evitar la guerra.", "Los ODS son un proyecto a 15 años para el mejoramiento de la calidad de vida humana velando por el sustento del planeta."],
["Comprencion Lectora", "El logro de los ODS (Objetivos de Desarrollo Sostenible) y el fomento de una prosperidad compartida son de interés para todos y ofrecen enormes oportunidades de inversión que beneficiarán a todas las personas y a nuestro planeta. Se trata de una agenda impulsada por las personas, construida sobre los cimientos de la transparencia, la participación y la inclusión. Los Objetivos son importantes para todos nosotros ya que tenemos una responsabilidad común con respecto a nuestro futuro y el de nuestro planeta. Sin objetivos ni metas claros para lograr resultados basados en datos empíricos, corremos el riesgo de dejar atrás a los más vulnerables y de no hacer frente adecuadamente a los nuevos desafíos que obstaculizan el desarrollo y dañan nuestro planeta. El título más adecuado para el párrafo anterior puede ser:", "La importancia de los ODS.", "El papel del PNUD en el proceso de los ODS.", "Los actores principales de los ODS.", "El seguimiento del progreso hacia los ODS.", "El texto trata de exponer las ventajas de los ODS, por lo cual, su objetivo no es otro mas que el de dar a entender la importancia de los mismos."],
["Comprencion Lectora", "Lo que hizo estuvo mal, por ello aún es atormentada por sus fantasmas del pasado. El la anterior oración, las palabras 'Fantasmas del pasado' se refieren a:", "Sus errores cometidos tiempo atrás.", "Un suceso paranormal que vivió en su niñez.", "Amigos de la infancia.", "Personas de su pasado que han muerto.", "El autor quiere dar a entender que la persona a la que es alusiva la oración cometió errores tiempo atrás por los cuales aún se ve afectada."],
["Comprencion Lectora", "De la frase 'uno crea sus propios demonios', podemos concluir que:", "Cada persona provoca sus propios problemas.", "Las personas a nuestro al rededor son responsables de nuestros problemas.", "Los problemas de cada persona son causados por demonios.", "La frase no tiene ningún sentido.", "Con 'demonios' la frase se refiere a los problemas o tragedias personales."],
["Comprencion Lectora", "Un sinónimo de la palabra 'sufragio' es:", "Votación.", "Sufrimiento.", "Azufre.", "Surfear.", "Sufragio: elección mediante votación de una opción entre varias que se presentan como candidatas."],
["Comprencion Lectora", "La definición de sinónimo es:", "Que tiene el mismo significado que otra u otras palabras o expresiones.", "Que tiene un significado opuesto o inverso al de otra palabra.", "Que o bien se escribe y se pronuncia o bien se pronuncia exactamente igual que otra pero tiene distinto significado y distinta etimología.", "Que se pronuncia exactamente igual que otra pero que se escribe de manera diferente y tiene distinto significado.", "Sinónimo. Definición: que tienen una mismo o muy parecido significado."],
["Comprencion Lectora", "En la oración 'Ya deja de ostentar tus insignificantes logros, hay muchos que lo hacen mejor que tú.' la palabra 'ostentar' puede ser reemplazada por:", "Presumir.", "Hablar.", "Comentar.", "Identificar.", "Ostentar: jactarse, vanagloriarse, pavonearse, ufanarse, engreírse, alardear, presumir, preciarse, alabarse."],
["Comprencion Lectora", "En la oración: 'No debes cohibirte de hacer el bien por miedo a hacerlo mal.' El verbo cohibir significa:", "Reprimir el deseo de hacer algo.", "Ocultar la verdadera intención de algo.", "Hacer todo mal.", "Intentar hacer algo.", "Cohibir. Definición: Impedir que una persona se comporte libremente o con naturalidad. Reprimirse o avergonzarse."],
["Matematicas", "¿Qué es un número Complejo?", "Son números que se expresan como la suma entre un número real y uno imaginario.", "Son números que pueden ser divididos entre sí mismos, la unidad y algún otro número entero positivo.", "Son números que únicamente pueden ser divididos entre la unidad y sí mismos.", "Son el conjunto de números comprendidos por los números negativos, positivos y el cero.", "Los números complejos son una extensión de los números reales y forman el mínimo cuerpo algebraicamente cerrado. Los números complejos incluyen todas las raíces de los polinomios, a diferencia de los reales. Todo número complejo puede representarse como la suma de un número real y un número imaginario (que es un múltiplo real de la unidad imaginaria, que se indica con la letra i), o en forma polar."],
["Matematicas", "¿Cuál de las siguientes afirmaciones es falsa?", "Los números decimales pertenecen al conjunto de los Números Enteros.", "El noventa y siete (97) es un número primo.", "Los números Naturales están contenidos en los Números Enteros.", "Los Números Irracionales son Infinitos.", "Se denominan números decimales a aquellos que poseen una parte decimal, y son opuestos a los números enteros que carecen de ella."],
["Matematicas", "¿Cuál de los siguientes grupos de letras pueden ser variables?", "Cualquier letra que represente una incógnita puede ser una variable.", "x, y, z.", "a, b, c.", "a, b, c, x, y, z.", "En matemáticas una variable es un símbolo cualquiera que se utiliza para expresar una cantidad que puede tomar distintos valores numéricos dentro de un conjunto de números especificado."],
["Matematicas", "Despeje la variable 'x' en la siguiente ecuación: 8x + 13y = 62", "x = (62 - 13y) / 8.", "x = (62 + 13y) / 8.", "x = (62 - 8y) / 13.", "x = 8 * (62 - 13y)", "Primero pasamos el término con la variable que no necesitamos despejar. 8x = - 13y + 62. Seguido pasamos la parte entera de 'x'. x = (- 13y + 62) / 8. Finalmente ordenamos los términos. x = (62 - 13y) / 8."],
["Matematicas", "¿En qué consiste el teorema de Pitágoras?", "El Teorema de Pitágoras dice que en cualquier triángulo rectángulo, la suma de los cuadrados de los catetos es igual al cuadrado de la hipotenusa.", "El Teorema de Pitágoras es una fórmula que proporciona el desarrollo de la potencia n-ésima (siendo n, entero positivo) de un binomio.", "El Teorema de Pitágoras explica esencialmente una forma de construir un triángulo semejante a uno previamente existente ('los triángulos semejantes son los que tienen ángulos congruentes, esto deriva en que sus lados homólogos sean proporcionales y viceversa').", "El Teorema de Pitágoras es un enunciado que describe la distribución asintótica de los números primos.", "Teorema de Pitágoras:  En todo triángulo rectángulo el cuadrado de la hipotenusa es igual a la suma de los cuadrados de los catetos."],
["Matematicas", "¿Cuál de los siguientes números pertenece al conjunto de los Números Enteros?", "- 34/17.", "97/11.", "13/2.", "15/9", "Un número entero es un elemento del conjunto numérico que contiene los números naturales, sus inversos aditivos y el cero. Los Números Enteros no poseen una parte decimal, de ahí su nombre."],
["Matematicas", "En la siguiente ecuación, ¿Qué valores no puede tomar la variable 'x'?: (7y) - (8/x) = 11", "0.", "Ningún Entero Negativo.", "8.", "- 8.", "La variable 'x' no puede tomar el valor cero pues la división entre éste número no está definida matemáticamente."],
["Matematicas", "¿Qué es una Asíntota?", "Es una recta prolongada indefinidamente a la que la curva de una función se acerca pero nunca llega a tocar.", "Es el punto máximo de una función exponencial.", "Es el punto donde las rectas de dos ecuaciones se interceptan.", "Es el punto (0,0) en el plano cartesiano.", "En matemática, se le llama asíntota de la gráfica de una función, a una recta a la que se aproxima continuamente la gráfica de tal función, con la condición de que estas jamás lleguen a interceptarse."],
["Matematicas", "¿Cuál es el valor de 'y' en la siguiente ecuación si 'x = 1'? 7y - 21x = 49", "y = 10.", "y = 4.", "y = 1.", "y = 7.", "Reemplazamos 'x = 1'. 7y - 21(1) = 49. Operamos y despejamos la 'y'. y = 10."],
["Matematicas", "¿Cuál es el resultado de la siguiente operación? '9 - 3 * 3 + 4'", "4.", "22.", "42.", "12.", "Según la jerarquía de operaciones, cuando no existe agrupación de términos, primero se resuelven de Izquierda a Derecha las Raíces y Potencias, seguido las Multiplicaciones y Divisiones y finalmente las Adiciones y Sustracciones."],
["Matematicas", "¿Cuál de las siguientes igualdades es Correcta?", "7 * (2x - 7y) =  14x - 49y.", "2x - 7y = (14x + 49y) / 7.", "2x + 7y = 7 * (14x + 49y).", "(2x + 7y) / 7 = (14x + 49y)", "Resolviendo el paréntesis: 7 * 2x = 14x. 7 * (-7y) = -49y. Por consiguiente: 7 * (2x - 7y) =  14x - 49y."],
["Matematicas", "Según la ley de los signos es correcto afirmar que:", "El producto de signos iguales tiene resultado positivo.", "El producto de signos diferentes tiene resultado positivo.", "El producto de signos diferentes depende del signo del número con mayor valor absoluto.", "El producto de signos iguales tiene resultado negativo.", "El producto de signos iguales, sean positivos o negativos, siempre resultará en un número positivo. (+ x + = +) (- x - = +)."],
["Matematicas", "La raíz cuadrada de un número negativo no está definida porque:", "No existe forma de multiplicar dos número con igual signo y que estos den un valor negativo.", "Al multiplicar un número por sí mismo, siempre dará negativo.", "Al elevar un número negativo al cuadrado su signo depende de si éste es par o impar.", "Las raíces cuadradas siempre tienen que dar como resultado un número entero positivo.", "La raíz cuadrada está definida únicamente para todo número mayor o igual que cero pues, según la ley de los signos, no hay forma de que al multiplicar un número ya sea positivo o negativo por sí mismo, se obtenga como resultado un número negativo."],
["Matematicas", "Cuando el exponente de una potencia es negativa, podemos:", "Cambiar el signo del exponente y la base por su inverso.", "Cambiar el signo del exponente y dividir la base entre el mismo exponente.", "Cambiar el signo del exponente y de la base.", "Cambiar el signo del exponente y multiplicar la base por el mismo exponente.", "Para eliminar un exponente negativo, basta con reemplazar la base por su número inverso. Por ejemplo el inverso de un número 'x' sería '1/x' y el inverso de '2/3' sería '3/2'; y posteriormente eliminar el signo negativo del exponente."],
["Matematicas", "¿Qué es una ecuación lineal?", "Es una ecuación donde todas sus variables se encuentran a la primera potencia.", "Es una ecuación en la que únicamente la variable dependiente se encuentra a la primera potencia.", "Es una ecuación en la que únicamente la variable independiente se encuentra a la primera potencia.", "Es una ecuación es la que la variable independiente, sin importar su potencia, se encuentran acompañada por un número natural.", "Una ecuación de primer grado o ecuación lineal es una igualdad que involucra una o más variables a la primera potencia y no contiene productos entre las variables, es decir, una ecuación que involucra solamente sumas y restas de una variable a la primera potencia."],
["Matematicas", "¿Cuál es la variable dependiente en la siguiente ecuación? x = 4y - 2.", "x.", "y.", "Ambas son variables dependientes.", "Ninguna es una variable dependiente.", "La variable dependiente es aquella cuyo valor depende del valor numérico que adopta la variable independiente en la función. Por lo tanto, al 'x' depender del valor que le sea asignado a 'y', es la variable dependiente."],
["Matematicas", "Para la solución de sistemas de ecuaciones dos por dos (Dos ecuaciones dos incógnitas), uno de los métodos usados es el de sustitución, el cual consiste en:", "Despejar una variable en la primera ecuación y reemplazarla en la segunda.", "Despejar la misma variable en ambas ecuaciones e igualarlas.", "Multiplicar una o ambas ecuaciones por un número de tal modo que nos permita eliminar un término al sumar algebraicamente ambas ecuaciones.", "Despejar una variable en la primera ecuación y reemplazarla en la misma ecuación una vez despejada.", "El nombre de éste método es debido a que una vez hayamos despejado una de las variables en cualquiera de las dos ecuaciones, debemos proceder a sustituirla en la ecuación contraria, para así obtener el valor de la variable que no hemos despejado."],
["Matematicas", "¿Cuál es el valor de 'x' en la siguiente ecuación si 'y = 1/2'? 2x - 3y = 11", "25/4.", "11/5.", "- 1/11.", "- 25/4", "Tras despejar la ecuación y operar los valores enteros: x = (11 - 3 y) / 2. Luego reemplazamos y = 1/2. x = [11 + (3/2) ] / 2. Finalmente realizamos las operaciones necesarias: x = 25/4."],
["Matematicas", "En la siguiente ecuación ¿En qué valor de 'x' se presenta una asíntota? y = 1/(x-8).", "8.", "0.", "- 8.", "1.", "Si 'x' toma el valor de 8, y = 1/(8 - 8). Es decir: y = 1/0. Como la división entre cero no está definida aritméticamente, 'x' no puede tomar el valor de 8, por lo tanto se presenta una asíntota paralela al eje Y."],
["Matematicas", "¿Cuál de las siguiente afirmaciones es correcta?", "(- 4) > (- 1).", "(- 1) = 1.", "0 < (- 3).", "(- 11) > 11", "Para los números negativos, entre mayor sea su valor absoluto, menor será su valor en la recta numérica."]]

        for i in listaq:
            #print(i)
            qq = MCQuestion()
            #qq.quiz =
            #qq.category = i[0] # poner un get
            #qq.sub_category =
            qq.content = i[1]
            qq.explanation = i[6]
            qq.answer_order = 'random'
            qq.save()

            aw0 = Answer()
            aw0.question = qq
            aw0.content = i[2]
            aw0.correct = True
            aw0.save()

            aw1 = Answer()
            aw1.question = qq
            aw1.content = i[3]
            aw1.correct = False
            aw1.save()

            aw2 = Answer()
            aw2.question = qq
            aw2.content = i[4]
            aw2.correct = False
            aw2.save()

            aw3 = Answer()
            aw3.question = qq
            aw3.content = i[5]
            aw3.correct = False
            aw3.save()

            #if  i[0] == "Ciencias Naturales":
            #    qq.category = c3
            #   qq.quiz.add(qq3)
            #else if i[0] == "Ciencias Sociales":
            #    qq.category = ct2
            #    #qq.quiz.add(qq2)
            #else if i[0] == "Comprencion Lectora":
            #    qq.category = ct1
            #    #qq.quiz.add(qq1)
            #else if i[0] == "Matematicas":
            #    qq.category = ct0
            #   qq.quiz.add(qq0)

        print("fin configuracion")
    else:
        print("ya esta configurado -- creo")

#initConfiguraciones()
