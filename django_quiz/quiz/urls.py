from django.conf.urls import patterns, url

from .views import QuizListView, CategoriesListView,\
    ViewQuizListByCategory, QuizUserProgressView, QuizMarkingList, QuizMarkingList1,\
    QuizMarkingDetail, QuizDetailView, QuizTake, QuizUserProgressDetail, my_final_result_user, user_list, results3


urlpatterns = patterns('',

                       url(regex=r'^$',
                           view=QuizListView.as_view(),
                           name='quiz_index'),

                       url(regex=r'^category/$',
                           view=CategoriesListView.as_view(),
                           name='quiz_category_list_all'),

                       url(regex=r'^category/(?P<category_name>[\w|\W-]+)/$',
                           view=ViewQuizListByCategory.as_view(),
                           name='quiz_category_list_matching'),

                       url(regex=r'^progress/$',
                           view=QuizUserProgressView.as_view(),
                           name='quiz_progress'),

                       url(regex=r'^progress/(?P<username>\d+)$',
                           view=QuizUserProgressDetail.as_view(),
                           name='quiz_progress'),

                       url(regex=r'^marking/$',
                           view=QuizMarkingList.as_view(),
                           name='quiz_marking'),

                       url(regex=r'^marking1/$',
                           view=QuizMarkingList1.as_view(),
                           name='quiz_marking'),

                       url(regex=r'^result/$',
                           view=user_list,
                           name='results'),

                       url(regex=r'^result3/$',
                           view=results3,
                           name='results3'),

                       url(regex=r'^result/(?P<user_name>[\w.@+-]+)/$',
                           view=my_final_result_user,
                           name='result'),

                       url(regex=r'^marking/(?P<pk>[\d.]+)/$',
                           view=QuizMarkingDetail.as_view(),
                           name='quiz_marking_detail'),

                       #  passes variable 'quiz_name' to quiz_take view
                       url(regex=r'^(?P<slug>[\w-]+)/$',
                           view=QuizDetailView.as_view(),
                           name='quiz_start_page'),

                       url(regex=r'^(?P<quiz_name>[\w-]+)/take/$',
                           view=QuizTake.as_view(),
                           name='quiz_question'),
)
