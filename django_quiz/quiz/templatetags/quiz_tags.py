from django import template

from quiz.models import Quiz, Category, Progress, Sitting, Question

register = template.Library()


@register.inclusion_tag('correct_answer.html', takes_context=True)
def correct_answer_for_all(context, question):
    """
    processes the correct answer based on a given question object
    if the answer is incorrect, informs the user
    """
    answers = question.get_answers()
    incorrect_list = context.get('incorrect_questions', [])
    if question.id in incorrect_list:
        user_was_incorrect = True
    else:
        user_was_incorrect = False

    return {'previous': {'answers': answers},
            'user_was_incorrect': user_was_incorrect}

@register.inclusion_tag('correct_answer.html', takes_context=True)
def correct_answer_for_all_1(context, question, sit):
    """
    processes the correct answer based on a given question object
    if the answer is incorrect, informs the user
    """
    answers = question.get_answers()
    #incorrect_list = context.get('incorrect_questions', [])
    incorrect_list = get_the_incorrect_questions(sit, sit)
    if question.id in incorrect_list:
        user_was_incorrect = True
    else:
        user_was_incorrect = False

    return {'previous': {'answers': answers},
            'user_was_incorrect': user_was_incorrect}


@register.filter
def answer_choice_to_string(question, answer):
    return question.answer_choice_to_string(answer)


@register.filter
def get_the_questions(sitting):
    return sitting.get_questions(with_answers=True)

@register.filter
def get_the_incorrect_questions(sitting, answer):
    return sitting.get_incorrect_questions


@register.filter
def porcentage(num1, nun2):
    return int((num1/nun2)*100)

@register.filter
def sumList(myList):
    return sum(myList)

@register.filter
def get_Message(num):
    if num >= 85:
        return "Muy Bien"
    if num >= 60:
        return "Bien"
    if num < 60:
        return "Mal"

@register.filter
def get_NameQuiz(num):
    #question = User.objects.get(username=name)
    return Quiz.objects.get(id=num).title


@register.simple_tag
def muiltiply(a, b):
    return a*b

@register.simple_tag
def suma(a, b):
    return a+b

@register.simple_tag
def divide(a, b):
    return a/b

@register.simple_tag
def minus(a, b):
    return a-b

@register.simple_tag
def get_incorrectas(numPreguntas, numQuizes, numCorrecto):
    return (numQuizes*numPreguntas)-numCorrecto

@register.simple_tag
def get_porcentage(numPreguntas, numQuizes, numCorrecto):
    return int((numCorrecto/(numQuizes*numPreguntas))*100)
