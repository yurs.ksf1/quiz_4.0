from django.conf.urls import patterns, url

from .views import myview


urlpatterns = patterns('',

                       url(regex=r'^$',
                           view=myview,
                           name='myview'),

                       url(regex=r'^myview/$',
                           view=myview,
                           name='myview'),
)
