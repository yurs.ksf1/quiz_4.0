from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    # Examples:
    # url(r'^$', 'mysite.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^', include('common.urls')),

    url(r'^pma/', include('quiz.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^auth/', include('django.contrib.auth.urls')),
    url(r'^accounts/',          include('registration.backends.default.urls')),
    #url(r'^myapp/',          include('myapp.urls')),




]
